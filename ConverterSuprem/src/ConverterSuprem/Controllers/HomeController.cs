﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterSuprem.Models;

namespace ConverterSuprem.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Suprem";
            ViewData["Result"] = "";
            ViewData["Value"] = "";

            Temperature converter = new Temperature();
            return View(converter);
        }

        public IActionResult Convert(Temperature converter)
        {
            double result;
            if (ModelState.IsValid)
            {
                result = (converter.Temperature_F - 32) * 5.0 / 9.0;
                ViewData["Title"] = "Converted by Suprem";
                ViewData["Value"] = result;
                ViewData["Result"] = "Temperature in C = " +
                    (int)(result);
            }
            return View("Index", converter);
        }

    }
}
